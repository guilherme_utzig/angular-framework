import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MasterComponent } from './master.component';
import { IndexModule } from './index/index.module';
import { SharedModule } from '../shared/shared.module';
import { AppComponent } from '../app.component';
import { AppRoutingModule } from '../app-routing.module';

@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    IndexModule,
    SharedModule
  ],
  declarations: [MasterComponent]
})
export class MasterModule { }
