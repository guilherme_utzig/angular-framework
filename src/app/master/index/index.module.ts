import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index.component';
import { ArticleComponent } from './article/article.component';
import { SliderComponent } from './slider/slider.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    IndexComponent,
    ArticleComponent,
    SliderComponent
  ],
  exports: [
    IndexComponent
  ]
})
export class IndexModule { }
