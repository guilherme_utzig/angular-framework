import { Component, OnInit } from '@angular/core';
import { ArticleComponent } from './article/article.component';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.styl']
})
export class IndexComponent implements OnInit {

  public articles = [
    {
      title: 'Bloco 1',
      description: 'Lorem ipsum dolor amet'
    },
    {
      title: 'Bloco 2',
      description: 'Lorem ipsum dolor amet'
    },
    {
      title: 'Bloco 3',
      description: 'Lorem ipsum dolor amet'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
