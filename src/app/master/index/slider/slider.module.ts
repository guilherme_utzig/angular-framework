import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SliderComponent } from './slider.component';
import { SlickModule } from 'ngx-slick';

@NgModule({
  imports: [
    CommonModule,
    SlickModule.forRoot()
  ],
  declarations: [
    SliderComponent
  ],
  exports: [
    SliderComponent
  ]
})
export class SliderModule { }
