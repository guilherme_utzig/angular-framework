import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.styl']
})
export class SliderComponent implements OnInit {

  slideConfig = { 'slidesToShow': 1, arrows: false, dots: true };

  constructor() { }

  ngOnInit() {
  }

}
