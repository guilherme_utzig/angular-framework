import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { MasterComponent } from './master/master.component';
import { IndexComponent } from './master/index/index.component';

const routes: Routes = [
  {
    path: '',
    component: MasterComponent,
    data: {
      breadcrumbs: 'Página Inicial',
      animation: { value: 'dashboard' }
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: IndexComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
